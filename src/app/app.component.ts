import { Component } from '@angular/core';
import { ModuleWalkerService, ExposedQuestion } from 'module-walker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'module-walker';

  privateModuleJson = '';
  question: ExposedQuestion = null;
  nodesLeft = 'Unset';
  questionstring = '';

  constructor(private walker: ModuleWalkerService) { }

  getAnswers() {
    console.log(JSON.stringify(this.walker.getAnswers()));
  }

  setModule() {
    console.log('setting module...');
    this.question = this.walker.setModule(JSON.parse(this.privateModuleJson));
    this.setProgress();
    this.setQuestionString();
    console.log(this.question);
  }

  enterFlow(ans: string) {
    this.question = this.walker.progressToNextNode(ans);
    this.setProgress();
    this.setQuestionString();
    console.log(this.question);
  }

  back() {
    this.question = this.walker.progressToPreviousNode();
    this.setProgress();
    this.setQuestionString();
    console.log(this.question);
  }

  setProgress() {
    this.nodesLeft = `${this.walker.getProgress().toString()} %`;
  }

  setQuestionString() {
    const lincdata = this.question.lincData.find((o) => o.key === 'question');
    if (lincdata) {
      this.questionstring = lincdata.value;
    } else {
      this.questionstring = this.question.value;
    }
  }
}
