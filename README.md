# ModuleWalker

A library used to interact with Suitless modules.

## Installation

You can simply install the angular module using NPM: `npm install module-walker`. The module is now ready for work.

## Usage

When using the library, there is no need to register any module. You can simply import the service using:
```typescript
import { ModuleWalkerService, QuestionWrapper } from 'module-walker';


constructor(private walker: ModuleWalkerService) { }
```
### Setting a module
All you need for setting a module, is the module JSON, these can be accuired from the `module-service`. You can then simply call the service to set your module.
```typescript
question: QuestionWrapper;
this.question = this.walker.setModule(JSON.parse({JSON}));
```

### Answering questions
The current question, contains an `answers` field, this will contain all possible flows that a user can take. If there are two answers, `yes` and `no`, the following example will fill in an answer.
```typescript
this.question = this.walker.progressToNextNode('no');
```
it is also possible for the current question to be a multiple choice question. For example, there are three answers `plastic`, `metal` and `wood`, the following example will enter two answers.
```typescript
this.question = this.walker.progressToNextNode(['metal', 'wood']);
```

### Getting the current process
The current progress is given in the form of nodes that are left, the maximum amount of nodes that could be left are given at the start of a module. The nodes left does not represent the real number of questions left, and should only be used to show the user a percentage of their progress. This progress does not grow linear, so it might be better to use a Logarithm. The current nodes left can be retrieve as follows:
```typescript
let nodesLeft: number = this.walker.getNodesLeft();
```

### Going back to previous questions
You can go back to a previous question using the following logic:
```typescript
this.question = this.walker.progressToPreviousNode();
```
