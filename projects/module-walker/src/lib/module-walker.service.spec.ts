import { ModuleWalkerService } from './module-walker.service';
import {
  IModule,
  IStartNode,
  ENodeType,
  IQuestionNode,
  IImplicationNode,
  IEndNode,
  INotificationNode
} from '@suitless/module-domain';

describe('ModuleWalkerService', () => {
  it('should load a module', () => {
    const module = new shortDemoModule();
    const service = new ModuleWalkerService();
    service.setModule(module);

    expect(service.state.currentNode).toBeTruthy();
    expect(service.state.currentNode.type).toBe(ENodeType.Start);
  });

  it('should walk through a question', () => {
    const module = new shortDemoModule();
    const service = new ModuleWalkerService();
    service.setModule(module);

    service.goForward();
    service.goForward([0]);

    expect(service.state.currentNode).toBeTruthy();
    expect(service.state.currentNode.type).toBe(ENodeType.Notification);
  });

  it('should force a forced end node', () => {
    const module = new shortDemoModule();
    const service = new ModuleWalkerService();
    service.setModule(module);

    service.goForward();
    service.goForward([1]);

    expect(service.state.currentNode).toBeTruthy();
    expect(service.state.currentNode.type).toBe(ENodeType.End);
  });

  it('should force a forced end node II', () => {
    const service = new ModuleWalkerService();
    service.setModule(
      JSON.parse(
        '{"id":"1","name":"testModule","imageUri":"","owner":"","description":"","version":1,"exposed":false,"tags":[],"nodes":[{"id":0,"type":0,"typeData":{"text":"Start.","description":"This is an example description.","flows":[1,6]}},{"id":1,"type":2,"typeData":{"text":"Is this a question?","multi":false,"useUniqueRoutes":false,"examples":[],"explanations":[],"answers":[{"id":2,"text":"Yes.","flows":[3]},{"id":4,"text":"No.","flows":[5]},{"id":11,"text":"Maybe.","flows":[12]}]}},{"id":3,"type":1,"typeData":{"text":"You are right.","force":false}},{"id":5,"type":1,"typeData":{"text":"You are wrong.","force":true}},{"id":6,"type":2,"typeData":{"text":"Another one?","multi":false,"useUniqueRoutes":false,"examples":[],"explanations":[],"answers":[{"id":7,"text":"Yes.","flows":[8]},{"id":9,"text":"No.","flows":[10]}]}},{"id":8,"type":1,"typeData":{"text":"You are correct.","force":false}},{"id":10,"type":1,"typeData":{"text":"You are wrong.","force":false}},{"id":12,"type":1,"typeData":{"text":"You are confused.","force":true}}]}'
      )
    );
    service.goForward();
    service.goForward([3]);

    expect(service.state.currentNode.type).toBe(ENodeType.End);
  });

  it('should work with multiple start flows', () => {
    const service = new ModuleWalkerService();
    service.setModule(
      JSON.parse(
        '{"id":"1","name":"testModule","imageUri":"","owner":"","description":"","version":1,"exposed":false,"tags":[],"nodes":[{"id":0,"type":0,"typeData":{"text":"Start.","description":"This is an example description.","flows":[1,6]}},{"id":1,"type":2,"typeData":{"text":"Is this a question?","multi":false,"useUniqueRoutes":false,"examples":[],"explanations":[],"answers":[{"id":2,"text":"Yes.","flows":[4]},{"id":3,"text":"No.","flows":[5]}]}},{"id":4,"type":1,"typeData":{"text":"You are right.","force":false}},{"id":5,"type":1,"typeData":{"text":"You are wrong.","force":true}},{"id":6,"type":2,"typeData":{"text":"Another one?","multi":false,"useUniqueRoutes":false,"examples":[],"explanations":[],"answers":[{"id":7,"text":"Yes.","flows":[9]},{"id":8,"text":"No.","flows":[10]}]}},{"id":9,"type":1,"typeData":{"text":"End.","force":false}},{"id":10,"type":1,"typeData":{"text":"End.","force":false}}]}'
      )
    );
    service.goForward();
    service.goForward([2]);
    service.goForward([7]);

    expect(service.state.currentNode.type).toBe(ENodeType.End);
  });
});

class shortDemoModule implements IModule {
  id: '0';
  imageUri = '';
  version = 1;
  name = 'testModule';
  tags = [];
  description = '';
  exposed = false;
  owner = '';
  nodes = [
    new startNode(),
    new vrNode(),
    new inactiveEndNode(),
    new forcedEndNode(),
    new notifNode(),
    new endNode()
  ];
}

class startNode implements IStartNode {
  id = 0;
  type = 0;
  typeData = {
    text: 'start',
    description: '',
    flows: [1, 4]
  };
}

class vrNode implements IQuestionNode {
  id = 1;
  type = 2;
  typeData = {
    multi: false,
    useUniqueRoutes: false,
    text: 'Question',
    explanations: [],
    examples: [],
    answers: [
      {
        id: 0,
        text: 'answer',
        flows: [2, 4]
      },
      {
        id: 1,
        text: 'answer',
        flows: [2, 5]
      }
    ]
  };
}

class inactiveEndNode implements IEndNode {
  id = 4;
  type = ENodeType.End;
  typeData = {
    text: 'End',
    force: false
  };
}

class forcedEndNode implements IEndNode {
  id = 5;
  type = ENodeType.End;
  typeData = {
    text: 'End',
    force: true
  };
}

class notifNode implements INotificationNode {
  id = 2;
  type = ENodeType.Notification;
  typeData = {
    text: 'Notification',
    flows: [3]
  };
}

class endNode implements IEndNode {
  id = 3;
  type = ENodeType.End;
  typeData = {
    text: 'End',
    force: false
  };
}
