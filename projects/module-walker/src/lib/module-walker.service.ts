import { Injectable } from '@angular/core';
import { find, cloneDeep, remove } from 'lodash-es';
import {
  IState,
  IModule,
  ENodeType,
  INode,
  IImplicationNode,
  IReportContent,
  IQuestionNode,
  IStartNode,
  IEndNode,
  EReportContentType,
  INotificationNode
} from '@suitless/module-domain';

@Injectable({
  providedIn: 'root'
})
/**
 * exposes a ModuleWalkerService which can be used to traverse V2 Modules.
 */
export class ModuleWalkerService {
  // TODO: Save history and state in LocalStorage to save local progress.
  history: IState[] = [];
  state: IState;

  constructor() {}

  /**
   * Sets the current IModule and clears the state
   * @param module new IModule
   */
  public setModule(module: IModule): INode {
    try {
      const node = find(module.nodes, { type: ENodeType.Start });
      if (node == null) {
        throw new Error('No start node was found.');
      }
      this.state = {
        module,
        currentNode: node,
        reportContent: [],
        stack: []
      };
      return node;
    } catch (error) {
      let errorNode = this.generateEndNode((error as Error).message);
      this.state.currentNode = errorNode;
      return errorNode;
    }
  }

  /**
   * Follows a flow given in a node, advances the state.
   * @param answerId (optional) The ID of the given answer
   */
  public goForward(answers?: number[]): INode {
    try {
      // Make a restore point for the goback function.
      this.history.push(cloneDeep(this.state));
      // Make a temporary stack, used to skip non-interactive nodes.
      let futureStack: number[] = [];
      let onlyUnique = false;

      // Interact with interactive nodes.
      switch (this.state.currentNode.type) {
        case ENodeType.Start:
        case ENodeType.Notification:
          // Follow the flows node.
          futureStack = (this.state.currentNode as IStartNode).typeData.flows;
          break;

        case ENodeType.Question:
          const questionNode = this.state.currentNode as IQuestionNode;
          if (questionNode.typeData.multi) {
            if (!answers || answers.length === 0) {
              throw new Error('The current node expects one or more answers.');
            }
            // Follow the flows specified in the answers
            futureStack = this.followMQuestionNode(answers);
            if (questionNode.typeData.useUniqueRoutes) {
              onlyUnique = true;
            }
            // Push the question to the report.
            this.state.reportContent.push({
              text: questionNode.typeData.text,
              type: EReportContentType.Question
            });
            // Push the answers to the report.
            for (const answer of answers) {
              const mQuestionAnswer = find(questionNode.typeData.answers, {
                id: answer
              });
              this.state.reportContent.push({
                text: mQuestionAnswer.text,
                type: EReportContentType.Answer
              });
            }
          } else {
            if (answers.length !== 1) {
              throw new Error('The current node expects exactly one answer.');
            }
            futureStack = this.followQuestionNode(answers[0]);
            // Follow the flows specified in the answer
            const questionAnswer = find(questionNode.typeData.answers, {
              id: answers[0]
            });
            // Push the question and answer to the report.
            this.state.reportContent.push({
              text: questionNode.typeData.text,
              type: EReportContentType.Question
            });
            this.state.reportContent.push({
              text: questionAnswer.text,
              type: EReportContentType.Answer
            });
          }
          break;

        default:
          throw new Error(
            `Navigating from nodeType ${
              ENodeType[this.state.currentNode.type]
            } is not supported.`
          );
          break;
      }

      // Resolve all non interactive nodes.
      this.state.stack = this.state.stack.concat(
        this.resolveNonInteractive(futureStack, onlyUnique)
      );
      this.sortStack(this.state.stack);

      // Skip to the next interactive node.
      let id = this.state.stack.shift();
      let node = find(this.state.module.nodes, { id });
      if (node) {
        this.state.currentNode = node;
      } else {
        throw new Error(
          'A dead-end has been hit, and no node could be resolved.'
        );
      }
      return this.state.currentNode;
    } catch (error) {
      let errorNode = this.generateEndNode((error as Error).message);
      this.state.currentNode = errorNode;
      return errorNode;
    }
  }

  /**
   * Returns to a previous state.
   */
  public goBack(): INode {
    this.state = this.history.pop();
    return this.state.currentNode;
  }

  /**
   * Returns the content that can be processed into the report.
   */
  public getReportContent(): IReportContent[] {
    return this.state.reportContent;
  }

  /**
   * Removed duplicate flows
   * @param flows incoming flows
   */
  private removeDuplicateFlows(flows: number[]): number[] {
    const result: number[] = [];
    for (const flow of flows) {
      if (!result.includes(flow)) {
        result.push(flow);
      }
    }
    return result;
  }

  /**
   * Resolves all non interactive nodes from the stack.
   * @param stack incoming stack
   */
  private resolveNonInteractive(
    stack: number[],
    onlyUnique: boolean
  ): number[] {
    let result: number[] = [];
    if (onlyUnique) {
      stack = this.removeDuplicateFlows(stack);
    }

    while (stack.length > 0) {
      // Stack should be sorted on node types.
      this.sortStack(stack);

      for (const id of stack) {
        stack = remove(stack, (value, index, array) => {
          return !(value === id);
        });
        const node = find(this.state.module.nodes, { id });

        switch (node.type) {
          case ENodeType.End:
            const endNode = node as IEndNode;
            if (
              endNode.typeData.force ||
              (stack.length == 0 && result.length == 0)
            ) {
              return [node.id];
            }
            break;
          case ENodeType.Implication:
            const implicationNode = node as IImplicationNode;
            stack = stack.concat(implicationNode.typeData.flows);
            this.state.reportContent.push({
              text: implicationNode.typeData.text,
              type: EReportContentType.Implication
            });
            break;

          case ENodeType.Compare:
          case ENodeType.Alter:
            throw new Error('Alter or Compare nodes are not supported yet.');
            break;

          default:
            // This is an interactive node.
            result.push(id);
            break;
        }
      }
    }
    return result;
  }

  private followQuestionNode(answerID: number): number[] {
    const node = this.state.currentNode as IQuestionNode;
    const answer = find(node.typeData.answers, { id: answerID });
    if (!answer) {
      throw new Error(`The given answer ID ${answerID} was not found.`);
    }
    return answer.flows;
  }

  private followMQuestionNode(answers: number[]): number[] {
    const node = this.state.currentNode as IQuestionNode;
    let result: number[] = [];
    for (const answerId of answers) {
      const answer = find(node.typeData.answers, { id: answerId });
      if (!answer) {
        throw new Error(`The given answer ID ${answerId} was not found.`);
      }
      if (node.typeData.useUniqueRoutes) {
        result = result.concat(this.removeDuplicateFlows(answer.flows));
      } else {
        result = result.concat(answer.flows);
      }
    }
    return result;
  }

  private generateEndNode(message: string): IEndNode {
    let endnode: IEndNode = {
      id: -1,
      type: ENodeType.End,
      typeData: {
        force: true,
        text: 'ERROR: ' + message
      }
    };
    return endnode;
  }

  private sortStack(stack: number[]) {
    // End nodes should be shifted to the end of the stack.
    // forced end nodes should be handles first.
    stack = stack.sort((a, b) => {
      const anode = find(this.state.module.nodes, { id: a }) as INode;
      const bnode = find(this.state.module.nodes, { id: b }) as INode;
      if (anode.type == ENodeType.End) {
        const endNode = anode as IEndNode;
        if (endNode.typeData.force) {
          return -1;
        } else {
          return 1;
        }
      }
      if (bnode.type == ENodeType.End) {
        const endNode = bnode as IEndNode;
        if (endNode.typeData.force) {
          return 1;
        } else {
          return -1;
        }
      }
      return 0;
    });
  }
}
